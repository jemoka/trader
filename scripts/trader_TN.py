# import os
# import sys 
# import csv
# import time
# import json
# import uuid
# import random
# import urllib
# import tflearn
# import logging
# import requests
# import threading
# import coloredlogs
# import numpy as np
# from tqdm import tqdm
# from queue import Queue
# import tensorflow as tf
# from decimal import Decimal




# # Part 0: Data Acquisitions
# def getDataPair():
#     tickerUrl = 'https://poloniex.com/public?command=returnTicker'
#     orderBookUrl = 'https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC'
#     currentTime = time.time()
#     oneHourAgo = currentTime-60*60
#     latestTradeUrl = 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=USDT_BTC&start='+str(oneHourAgo)+'&end='+str(currentTime)

#     try:
#         tickerRequest = requests.get(tickerUrl)
#         orderBookRequest = requests.get(orderBookUrl)
#         latestTradeRequest = requests.get(latestTradeUrl)

#         tickerData = np.array([Decimal(x).quantize(Decimal('.01')) for x in list(tickerRequest.json()["USDT_BTC"].values())])
#         orderBookData = orderBookRequest.json()
#         orderBookData_asks = np.array([[Decimal(y).quantize(Decimal('.001')) for y in x] for x in orderBookData["asks"]])
#         orderBookData_bids = np.array([[Decimal(y).quantize(Decimal('.001')) for y in x] for x in orderBookData["bids"]])
#         latestTrade = latestTradeRequest.json()[0]

#         bsIndex = 0

#         if latestTrade['type'] == 'buy':
#             bsIndex = [Decimal(0), Decimal(1)]
#         elif latestTrade['type'] == 'sell':
#             bsIndex = [Decimal(1), Decimal(0)]

#         preparedInputData = np.concatenate([tickerData, orderBookData_asks.flatten(), orderBookData_bids.flatten()]).tolist()
#         preparedOutputData = [Decimal(latestTrade['rate']).quantize(Decimal('.001')), Decimal(latestTrade['amount']).quantize(Decimal('.001'))]+bsIndex
    
#     except:
#         return getDataPair()

#     return preparedInputData, preparedOutputData

# # Part 0.5: Data Reading+Writing
# def writeData():
#     epoch = int(input("Write Items: "))
#     with open('agentTrainingData.inputs.csv', 'a', newline='') as inputDataFile, open('agentTrainingData.outputs.csv', 'a', newline='') as ouputDataFile:
#         inputWriter = csv.writer(inputDataFile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
#         outputWriter = csv.writer(ouputDataFile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

#         for i in tqdm(range(epoch)):
#             dataPair = getDataPair()
#             inputWriter.writerow(dataPair[0])
#             outputWriter.writerow(dataPair[1])
#             sys.stdout.flush() 

#     print("")

# def readData(n=None):
#     with open('agentTrainingData.inputs.csv', 'r', newline='') as inputDataFile, open('agentTrainingData.outputs.csv', 'r', newline='') as ouputDataFile:
#         inputReader = csv.reader(inputDataFile, delimiter=',', quotechar='|')
#         outputReader = csv.reader(ouputDataFile, delimiter=',', quotechar='|')

#         regressionInputData = []
#         priceData = []
#         orderData = []

#         c = 0

#         if n == None:
#             print("Loading data...")
#             for inputDataItem, outputDataItem in tqdm(zip(inputReader, outputReader)):
#                 idP1 = inputDataItem[:5]
#                 idP2 = inputDataItem[10:]
#                 regressionInputData.append(idP1+idP2)
#                 priceData.append(outputDataItem[:1])
#                 orderData.append(outputDataItem[1:])
#             print("Data loaded!")
#         else:
#             print("Loading data...")
#             for inputDataItem, outputDataItem in tqdm(zip(inputReader, outputReader), total=n):
#                 if n < c:
#                     break
#                 else:
#                     c += 1
#                 idP1 = inputDataItem[:5]
#                 idP2 = inputDataItem[9:]
#                 regressionInputData.append(idP1+idP2)
#                 priceData.append(outputDataItem[:1])
#                 orderData.append(outputDataItem[1:])
#             print("Data loaded!")

#         return regressionInputData, priceData, orderData

# # writeData()
# # d = readData(160000)
# # x = d[0]
# # y = d[1]
# # z = d[2]
# # x, y, z = tflearn.data_utils.shuffle(x, y, z)



# # def priceRegression(x):
# #     with tf.variable_scope('PriceRegression'):
# #         net = tflearn.layers.batch_normalization(x)
# #         net = tflearn.fully_connected(x, 150)
# #         net = tflearn.fully_connected(net, 75)
# #         net = tflearn.fully_connected(net, 35)
# #         net = tflearn.fully_connected(net, 1, activation="ReLU")
# #         return net

# # def orderRegression(x):
# #     with tf.variable_scope('OrderRegression'):
# #         net = tflearn.layers.batch_normalization(tflearn.fully_connected(x, 35))
# #         net = tflearn.fully_connected(net, 75)
# #         net = tflearn.fully_connected(net, 20)
# #         net = tflearn.fully_connected(net, 3, activation="sigmoid")
# #         return net

# def priceRegression(x):
#     with tf.variable_scope('PriceRegression'):
#         net = tflearn.fully_connected(x, 25)
#         net = tflearn.fully_connected(net, 75)
#         net = tflearn.fully_connected(net, 15)
#         net = tflearn.fully_connected(net, 1, activation="ReLU")
#         return net

# def orderRegression(x):
#     with tf.variable_scope('OrderRegression'):
#         net = tflearn.layers.batch_normalization(tflearn.fully_connected(x, 35))
#         net = tflearn.fully_connected(net, 15)
#         net = tflearn.fully_connected(net, 5)
#         net = tflearn.fully_connected(net, 3, activation="sigmoid")
#         return net


# def mae(pred, true):
#     return tf.reduce_mean(tf.abs(true - pred))

# # Part 1: Graph

# # with tf.Graph().as_default() as g:
# marketDataInput = tflearn.input_data(shape=[None, 206])
# expectedPrice = tf.placeholder(tf.float32, shape=[None, 1])
# expectedOrder = tf.placeholder(tf.float32, shape=[None, 3])
# predictedPrice = priceRegression(marketDataInput)
# predictedOrder = orderRegression(tflearn.merge_outputs([predictedPrice, marketDataInput]))

# fullPredictedOrder = tflearn.merge_outputs([predictedPrice, predictedOrder])

# priceLoss = mae(predictedPrice, expectedPrice)
# orderLoss = mae(predictedOrder, expectedOrder)

# priceOptimizer = tflearn.SGD(learning_rate=8e-4, lr_decay=0.98, decay_step=500)
# orderOptimizer = tflearn.SGD(learning_rate=5e-3, lr_decay=0.95, decay_step=5000)

# # priceOptimizer = tflearn.SGD(learning_rate=2e-5, lr_decay=0.80, decay_step=1000)
# # orderOptimizer = tflearn.SGD(learning_rate=5e-3, lr_decay=0.95, decay_step=800)

# priceRegModel = tflearn.regression(
#     predictedPrice,
#     placeholder=None, 
#     optimizer=priceOptimizer, 
#     loss=priceLoss, 
#     trainable_vars=tflearn.get_layer_variables_by_scope('PriceRegression'),
#     batch_size=20, 
#     name='price_model', 
#     op_name='PRICE',
# )

# orderRegModel = tflearn.regression(
#     fullPredictedOrder,
#     placeholder=None, 
#     optimizer=orderOptimizer,
#     loss=orderLoss,
#     trainable_vars=tflearn.get_layer_variables_by_scope('OrderRegression'),
#     batch_size=20,
#     name='order_model',
#     op_name='ORDER',
# )

# orderModel = tflearn.DNN(orderRegModel)


# orderModel.load("D:\\Users\\kmliu\\Documents\\Developing Projects\\trader\\Trained Agents\\saved_models\\agentV3_0")

# # odp = None
# # for _ in range(100):
# #     dp = getDataPair()
# #     while odp == dp[1]:
# #         dp = getDataPair()
# #     inputData, outputData = dp
# #     idP1 = inputData[:5]
# #     idP2 = inputData[9:]
# #     odp = dp[1]
# #     print(outputData)
# #     order = orderModel.predict([idP1+idP2])
# #     print(order)
# #     print("------------------------")

# # Part 2: Databasing Tools
# def initializeDB(n=50):
#     with urllib.request.urlopen('https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC') as response:
#         orderBook = json.load(response)

#     asks = orderBook["asks"][:n]
#     bids = orderBook["bids"][:n]

#     for i in asks:
#         price = float(i[0])
#         amount = i[1]

#         data = {
#             "price":round(price, 2), 
#             "coinAmount":round(amount, 10), 
#             "orderBook":3,
#             "type": 2,
#         }

#         r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 

#     for i in bids:
#         price = float(i[0])
#         amount = i[1]

#         data = {
#             "price":round(price, 2), 
#             "coinAmount":round(amount, 10), 
#             "orderBook":3,
#             "type": 3,
#         }

#         r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data)

# def getPortfolioValue(coin, fund, pk):
#     tickerUrl = 'http://127.0.0.1:8000/api/USDT_BTC/ticker/?pk='+str(pk)
#     try:
#         tickerData = requests.get(tickerUrl).json()
#         return Decimal(tickerData["last"])+Decimal(fund)
#     except:
#         return getPortfolioValue(coin, fund, pk)

# def getMarketData(pk):
#     marketUrl = 'http://127.0.0.1:8000/api/USDT_BTC/markets/'
#     tickerUrl = 'http://127.0.0.1:8000/api/USDT_BTC/ticker/?pk='+str(pk)

#     try:
#         tickerRequest = requests.get(tickerUrl)
#         marketRequest = requests.get(marketUrl)

#         tickerData = np.array([Decimal(x).quantize(Decimal('.01')) for x in list(tickerRequest.json().values())])
#         orderBookData_asks = np.array([[Decimal(y).quantize(Decimal('.001')) for y in x] for x in marketRequest.json()[0]["orderBooks"][0]["asks"]])[-50:]
#         orderBookData_bids = np.array([[Decimal(y).quantize(Decimal('.001')) for y in x] for x in marketRequest.json()[0]["orderBooks"][0]["bids"]])[-50:]

#         preparedInputData = np.concatenate([np.array([Decimal(121)]), tickerData, orderBookData_asks.flatten(), orderBookData_bids.flatten()]).tolist()
        
#         idP1 = preparedInputData[:5]
#         idP2 = preparedInputData[9:]

#         if len(preparedInputData) < 210:
#             initializeDB(210-len(preparedInputData))
#             return getMarketData(pk)

#     except:
#         return getMarketData(pk)

#     return idP1+idP2

# # Part 3: RL Tools
# funding = 10000
# btc = 1
# def getNextStateReward(currentState, currentAction):
#     global funding
#     global btc

#     oldPrice = getPortfolioValue(1,0)

#     punishment = 0

#     price = Decimal(currentAction[0].item()).quantize(Decimal('.01'))
#     coinAmount = Decimal(currentAction[1].item()).quantize(Decimal('.001'))

#     if currentAction[2] > currentAction[3]:
#         if funding >= price*coinAmount:
#             data = {
#                 "price":str(price), 
#                 "coinAmount":str(coinAmount), 
#                 "orderBook":3,
#                 "type": 0,
#             }
#             r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
#             funding -= price*coinAmount
#             btc += coinAmount
#         else:
#             punishment = -1

#     elif currentAction[3] > currentAction[2]:
#         if btc >= coinAmount:
#             data = {
#                 "price":str(price), 
#                 "coinAmount":str(coinAmount), 
#                 "orderBook":3,
#                 "type": 1,
#             }
#             r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
#             btc -= coinAmount
#             funding += price*coinAmount
#         else:
#             punishment = -1
#     time.sleep(0.005)
#     currentPrice = getPortfolioValue(1,0)
#     priceDelta = currentPrice-oldPrice
#     oldPrice = currentPrice
#     # sigmoid = lambda x: (math.e**float(x))/(1+math.e**float(x))
#     # tanh = lambda x: ((math.e**float(x))-(math.e**(-float(x))))/((math.e**float(x))+(math.e**(-float(x))))
#     reward = -priceDelta-punishment if -priceDelta >= 0 else 0.0-punishment

#     if reward == 0:
#         reward = -1

#     return getMarketData(), reward

# # Part 4: Trade
# def go_trade(tradeInterval=None, pk=0, ttl=2, reportingTime=1, startingFund=100, startingBTC=0.1, rQ=False):
#     agentID = str(uuid.uuid4())[-5:]
#     logging.info("Hello, I am agent ["+str(agentID)+"]")
#     logging.info("["+str(agentID)+"] "+"Assigned to thread: {}".format(threading.current_thread().name)) 
#     logging.info("["+str(agentID)+"] "+"ID of process running: {}".format(os.getpid())) 
#     funding = Decimal(startingFund).quantize(Decimal('.01'))
#     btc = Decimal(startingBTC).quantize(Decimal('.001'))
#     startTime = time.time()
#     lastReport = time.time()
#     unacceptedTrades = 0
#     trades = 0
#     prev = getPortfolioValue(btc, funding, pk)
#     first = prev
#     if not tradeInterval:
#         tradeInterval = random.uniform(0, 5)

#     while True:
#         if time.time() - lastReport >= reportingTime*60:
#             currentPortfolio = getPortfolioValue(btc, funding, pk)
#             logging.warning("["+str(agentID)+"] "+"Funding:" + str(funding)+ "  BTC:"+ str(btc)+ "  Trades:"+ str(trades)+ "  Portfolio Value:"+str(currentPortfolio.quantize(Decimal('.01')))+ "  Change:"+ str(((currentPortfolio-prev)/prev).quantize(Decimal('.01'))*100)+"%"+ "  Change (From Start):"+ str(((currentPortfolio-first)/first).quantize(Decimal('.01'))*100)+"%")
#             lastReport = time.time()
#             prev = currentPortfolio
        
#         order = orderModel.predict([getMarketData(pk)])[0]
#         price = Decimal(order[0].item()).quantize(Decimal('.01'))
#         coinAmount = Decimal(order[1].item()).quantize(Decimal('.001'))
#         if rQ == True:
#             orderType = random.sample([[0, 1], [1, 0]], 1)
#             order = order[:2].tolist()+orderType[0]
#         if order[2] > order[3]:
#             logging.debug("["+str(agentID)+"] "+"Attempting Buy...")
#             if funding >= price*coinAmount:
#                 data = {
#                     "price":str(price), 
#                     "coinAmount":str(coinAmount), 
#                     "orderBook":0,
#                     "type": 0,
#                 }
#                 r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
#                 funding -= price*coinAmount
#                 btc += coinAmount
#                 logging.info("["+str(agentID)+"] "+"Bidding order booked @ USDT"+str(price)+" for "+str(coinAmount)+" BTC.")
#                 trades += 1
#             else:
#                 unacceptedTrades += 1

#         elif order[3] > order[2]:
#             logging.debug("["+str(agentID)+"] "+"Attempting Sell...")
#             if btc >= coinAmount:
#                 data = {
#                     "price":str(price), 
#                     "coinAmount":str(coinAmount), 
#                     "orderBook":0,
#                     "type": 1,
#                 }
#                 r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
#                 btc -= coinAmount
#                 funding += price*coinAmount
#                 logging.info("["+str(agentID)+"] "+"Asking order booked @ USDT"+str(price)+" for "+str(coinAmount)+" BTC.")
#                 trades += 1
#             else:
#                 unacceptedTrades += 1

#         if time.time() - startTime >= ttl*60:
#             break
#         time.sleep(tradeInterval*60)
#     logging.info("Agent "+"["+str(agentID)+"] "+"Done!")
#     logging.info("["+str(agentID)+"] "+"is now finished with thread: {}".format(threading.current_thread().name)) 

# def multithread_traders(count, tradeInterval=None, ttl=2, reportingTime=1, startingFund=100, startingBTC=0.1, rQ=False, logName="trader_TN.log"):
#     print("Hello, I am the multithreaded agent spawner")
#     print("ID of process running main program: {}".format(os.getpid())) 
#     print("Main thread name: {}".format(threading.main_thread().name)) 

#     logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(message)s', filename=logName)
#     logger = logging.getLogger(__name__)
#     coloredlogs.install()

#     listOfThreads = []
#     for i in range(count):
#         listOfThreads.append(threading.Thread(target=go_trade, args=(tradeInterval, 0, ttl, reportingTime, startingFund, startingBTC, rQ)))

#     for thread in listOfThreads:
#         time.sleep(random.uniform(0, 5))
#         thread.start()

#     for thread in listOfThreads:
#         thread.join()

#     print("All processes finished...")


# multithread_traders(int(input("Market Count: ")), None, 100000000000000, 1, 1000, 2, True)

import os
import sys 
import csv
import time
import math
import json
import uuid
import shutil
import random
import urllib
import tflearn
import logging
import datetime
import tempfile
import requests
import threading
import coloredlogs
import numpy as np
from tqdm import tqdm
from queue import Queue
import tensorflow as tf
from operator import xor
from logger import Logger
from decimal import Decimal

def readData(n=None):
    with open('agentTrainingData.inputs.csv', 'r', newline='') as inputDataFile, open('agentTrainingData.outputs.csv', 'r', newline='') as ouputDataFile:
        inputReader = csv.reader(inputDataFile, delimiter=',', quotechar='|')
        outputReader = csv.reader(ouputDataFile, delimiter=',', quotechar='|')

        regressionInputData = []
        priceData = []
        orderData = []

        c = 0

        if n == None:
            print("Loading data...")
            for inputDataItem, outputDataItem in tqdm(zip(inputReader, outputReader)):
                regressionInputData.append(inputDataItem)
                priceData.append(outputDataItem[:1])
                orderData.append(outputDataItem[1:])
            print("Data loaded!")
        else:
            print("Loading data...")
            for inputDataItem, outputDataItem in tqdm(zip(inputReader, outputReader), total=n):
                if n < c:
                    break
                else:
                    c += 1
                idP1 = inputDataItem[:5]
                idP2 = inputDataItem[9:]
                regressionInputData.append(idP1+idP2)
                priceData.append(outputDataItem[:1])
                orderData.append(outputDataItem[1:2])
            print("Data loaded!")

        return regressionInputData, priceData, orderData

def getPortfolioValue(coin, fund, pk):
    tickerUrl = 'http://127.0.0.1:8000/api/USDT_BTC/ticker/?pk='+str(pk)
    try:
        tickerData = requests.get(tickerUrl).json()
        return Decimal(tickerData["last"])+Decimal(fund)
    except:
        return getPortfolioValue(coin, fund, pk)

def initializeDb(envSize):
    r = requests.get(url = "http://127.0.0.1:8000/api/USDT_BTC/reset/?type=1&env-amt="+str(envSize))

def fillOb(n=50, ob=0):
    with urllib.request.urlopen('https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC') as response:
        orderBook = json.load(response)

    asks = orderBook["asks"][:n]
    bids = orderBook["bids"][:n]

    for i in asks:
        price = float(i[0])
        amount = i[1]

        data = {
            "price":round(price, 2), 
            "coinAmount":round(amount, 10), 
            "orderBook":ob,
            "type": 2,
        }

        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 

    for i in bids:
        price = float(i[0])
        amount = i[1]

        data = {
            "price":round(price, 2), 
            "coinAmount":round(amount, 10), 
            "orderBook":ob,
            "type": 3,
        }

        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data)

def resetEnv(pk):
    r = requests.get(url = "http://127.0.0.1:8000/api/USDT_BTC/reset/?type=1&mk-pk="+str(pk))

def getMarketData(pk):
    marketUrl = 'http://127.0.0.1:8000/api/USDT_BTC/markets/'
    tickerUrl = 'http://127.0.0.1:8000/api/USDT_BTC/ticker/?pk='+str(pk)

    try:
        tickerRequest = requests.get(tickerUrl)
        marketRequest = requests.get(marketUrl)

        tickerData = np.array([Decimal(x).quantize(Decimal('.01')) for x in list(tickerRequest.json().values())])
        orderBookData_asks = np.array([[Decimal(y).quantize(Decimal('.001')) for y in x] for x in marketRequest.json()[0]["orderBooks"][0]["asks"]])[-50:]
        orderBookData_bids = np.array([[Decimal(y).quantize(Decimal('.001')) for y in x] for x in marketRequest.json()[0]["orderBooks"][0]["bids"]])[-50:]

        preparedInputData = np.concatenate([np.array([Decimal(121)]), tickerData, orderBookData_asks.flatten(), orderBookData_bids.flatten()]).tolist()
        
        idP1 = preparedInputData[:5]
        idP2 = preparedInputData[9:]

        if len(preparedInputData) < 210:
            fillOb(210-len(preparedInputData), pk)
            return getMarketData(pk)

    except:
        return getMarketData(pk)

    return idP1+idP2

class Environment(object):
    def __init__(self, pk=None, association=None, targetDecrease=0.1, avFund=10000, avCoin=1, waitPeriod=1, destroy=False):
        if association == None:
            requests.post("http://127.0.0.1:8000/api/USDT_BTC/markets/?pk="+str(pk))
            self.pk = pk
        elif pk == None:
            self.pk = association
        else:
            raise ValueError("Neither pk or association given.")
        self.avaliableFunds = avFund
        self.avaliableCoins = avCoin
        self.targetPrice = getPortfolioValue(1, 0, self.pk)*Decimal(1-targetDecrease)
        self.waitPeriod = waitPeriod
        self.d = False
        self.destroy = destroy

    def __del__(self):
        if self.pk and self.destroy: 
            requests.delete("http://127.0.0.1:8000/api/USDT_BTC/markets/?pk="+str(self.pk))

    def __reset(self):
        requests.get("http://127.0.0.1:8000/api/USDT_BTC/reset/?type=0&mk-pk="+str(self.pk))

    def getInitialState(self):
        return getMarketData(pk=self.pk)

    def getNextStateReward(self, currentAction):
        price = Decimal(currentAction[0].item()).quantize(Decimal('.01'))
        coinAmount = Decimal(currentAction[1].item()).quantize(Decimal('.001'))
        orderType = random.sample([[0, 1], [1, 0]], 1)
        currentAction = currentAction[:2].tolist()+orderType[0]
        
        initialPrice = getPortfolioValue(1, 0, self.pk)

        print(currentAction)

        if float(price) == 0 or float(coinAmount) <= 0:
            return getMarketData(pk=self.pk), -5, False

        if currentAction[2] > currentAction[3]:
            if float(self.avaliableFunds) >= float(price)*float(coinAmount):
                data = {
                    "price":str(price), 
                    "coinAmount":str(coinAmount), 
                    "orderBook":str(self.pk),
                    "type": 0,
                }
                r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
                self.avaliableFunds -= price*coinAmount
                self.avaliableCoins += coinAmount
            else:
                punishment = -1

        elif currentAction[3] > currentAction[2]:
            if float(self.avaliableCoins) >= float(coinAmount):
                data = {
                    "price":str(price), 
                    "coinAmount":str(coinAmount), 
                    "orderBook":str(self.pk),
                    "type": 1,
                }
                r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
                self.avaliableCoins -= coinAmount
                self.avaliableFunds += price*coinAmount
            else:
                punishment = -1

        time.sleep(self.waitPeriod)

        currentPrice = getPortfolioValue(1, 0, self.pk)

        targetPriceDeltaReward = self.targetPrice-currentPrice
        priceChangeReward = initialPrice-currentPrice

        normalizer = lambda x, y: ((y**float(x))-(y**(-float(x))))/((y**float(x))+(y**(-float(x))))

        if targetPriceDeltaReward >= 0:
            self.d = True
            return getMarketData(pk=self.pk), 2, True    
        
        if self.avaliableFunds <= 0:
            self.d = True
            return getMarketData(pk=self.pk), -2, True 
        
        punishment = 0
        if -1e-7 <= normalizer((targetPriceDeltaReward+priceChangeReward)/2, 1.001) <= 1e-7:
            punishment = -0.05

        return getMarketData(pk=self.pk), normalizer((targetPriceDeltaReward+priceChangeReward)/2, 1.001)+punishment, False

class Agent(object):
    def __init__(self, aType, enviroment, inputData, priceData, orderData, batchSize=None, ptEpoch=None, savePath="", ptLearningRate=(1e-9, 1e-13), tLearningRate=(1e-9, 1e-13), gamma=0.3, master=None):
        self.type = aType # 0 is master, 1 is slave
        self.graph = tf.Graph()
        self.rewardHistory = [0]
        self.env = enviroment
        with self.graph.as_default():
            self.inputData = tf.placeholder(dtype=tf.float32, shape=[None, 206], name="input")
            self.truePrice = tf.placeholder(dtype=tf.float32, shape=[None, 1], name="true_price")
            self.trueOrder = tf.placeholder(dtype=tf.float32, shape=[None, 1], name="true_order")
            self.memory = tf.placeholder(dtype=tf.float32, shape=[None], name="memory")
            self.reward = tf.placeholder(dtype=tf.float32, shape=1, name="reward")

            with tf.variable_scope('Actor'):
                with tf.variable_scope('PriceRegression'):
                    net = tflearn.fully_connected(self.inputData, 25)
                    net = tflearn.fully_connected(net, 15)
                    self.price = tflearn.fully_connected(net, 1, activation="ReLU")

                with tf.variable_scope('OrderRegression'):
                    net = tflearn.fully_connected(self.inputData, 75)
                    net = tflearn.fully_connected(net, 5)
                    net = tflearn.fully_connected(net, 15)
                    self.order = tflearn.fully_connected(net, 1, activation="ReLU")
                    
                self.action = tflearn.merge_outputs([self.price, self.order])

            with tf.variable_scope('Critic'):
                net = tflearn.fully_connected(self.action, 75, weights_init=tflearn.initializations.uniform(shape=None, minval=-1e-7, maxval=1e-7, dtype=tf.float32, seed=None))
                net = tflearn.fully_connected(net, 15, weights_init=tflearn.initializations.uniform(shape=None, minval=-1e-7, maxval=1e-7, dtype=tf.float32, seed=None))
                net = tflearn.fully_connected(net, 5, weights_init=tflearn.initializations.uniform(shape=None, minval=-1e-7, maxval=1e-7, dtype=tf.float32, seed=None))
                self.value = tflearn.fully_connected(net, 1, weights_init=tflearn.initializations.uniform(shape=None, minval=-1e-7, maxval=1e-7, dtype=tf.float32, seed=None), activation="linear")
            
            self.pt_priceLoss = tf.reduce_mean(tf.abs(self.truePrice - self.price))
            self.pt_orderLoss = tf.reduce_mean(tf.abs(self.trueOrder - self.order))

            self.pt_priceTrainOp = tf.train.GradientDescentOptimizer(ptLearningRate[0]).minimize(self.pt_priceLoss, var_list=tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="Actor/PriceRegression"))
            self.pt_orderTrainOp = tf.train.GradientDescentOptimizer(ptLearningRate[0]).minimize(self.pt_orderLoss, var_list=tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="Actor/OrderRegression"))

            R = tf.reduce_sum(self.memory)*gamma+self.reward
            self.advantage = self.value - tf.stop_gradient(R)
            self.t_policyLoss = -tf.reduce_sum(tf.multiply(tf.log(self.price), self.advantage), name="policy_loss")
            self.t_valueLoss = tf.reduce_mean(tf.square(self.advantage), name="value_loss")

            pt_priceSum = tf.summary.scalar('pt_priceSum', self.pt_priceLoss)
            pt_orderSum = tf.summary.scalar('pt_orderSum', self.pt_orderLoss)
            t_policySum = tf.summary.scalar('t_policySum', self.t_policyLoss)
            t_valueSum = tf.summary.scalar('t_valueSum', self.t_valueLoss)

            self.t_policyOp = tf.train.GradientDescentOptimizer(tLearningRate[0])
            self.t_valueOp = tf.train.GradientDescentOptimizer(tLearningRate[1])

            self.policyVars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="Actor/PriceRegression")
            self.criticVars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="Critic")

            self.t_policyGrads = tf.gradients(self.t_policyLoss, self.policyVars)
            self.t_valueGrads = tf.gradients(self.t_valueLoss, self.criticVars)

            self.summaryOp = tf.summary.merge_all()

            self.saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="Actor")+tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="Critic"))

            if self.type == 1:
                self.path = tempfile.mkdtemp()
            else:
                self.path = savePath

            self.currentPath = self.path

            if self.type == 0:
                self.writer = tf.summary.FileWriter(savePath, self.graph)

            self.sess = tf.Session()

            self.sess.run(tf.global_variables_initializer())

            if aType == 0:
                self._pre_train(inputData, priceData, orderData, batchSize, ptEpoch)
            else:
                self.loadGraph(master)

    def __del__(self):
        if self.type == 1:
            shutil.rmtree(self.path)

    def _chunker(self, seq, size):
        return (seq[pos:pos + size] for pos in range(0, len(seq), size))

    def _pre_train(self, inputData, priceData, orderData, batchSize, epoch=10):
        with self.graph.as_default():
            for e in range(epoch):
                ipd, opd, ood = tflearn.data_utils.shuffle(inputData, priceData, orderData)
                input_batches = self._chunker(ipd, batchSize)
                price_batches = self._chunker(opd, batchSize)
                order_batches = self._chunker(ood, batchSize)
                for i, (ip, price, order) in enumerate(zip(input_batches, price_batches, order_batches)):
                    _, _, priceLoss, orderLoss = self.sess.run([self.pt_priceTrainOp, self.pt_orderTrainOp, self.pt_priceLoss, self.pt_orderLoss], feed_dict={self.inputData: ip, self.truePrice: price, self.trueOrder: order})
                    if i%10 == 0:
                        print("Epoch", e, "batch", i, "error", (priceLoss+orderLoss)/2)
                        print("PriceLoss:", priceLoss, "OrderLoss:", orderLoss)
                        print("\n\n")
            time.sleep(random.uniform(0, 0.1))
            time.sleep(random.uniform(0, 0.1))

    def calcGrads(self, inputSlice):
        with self.graph.as_default():
            action, value = self.sess.run([self.action, self.value], feed_dict={self.inputData: [inputSlice]})
            action = action[0]
            value = value[0]
            nextState, reward, d = self.env.getNextStateReward(action)
            pGrads, vGrads, pLoss, vLoss = self.sess.run([self.t_policyGrads, self.t_valueGrads, self.t_policyLoss, self.t_valueLoss], feed_dict={self.inputData: [inputSlice], self.reward: [reward], self.memory: self.rewardHistory})
            print("Loss: ", (pLoss+vLoss)/2)
            print("PriceLoss:", pLoss, "ValueLoss:", vLoss)
            print("\n\n")
            self.rewardHistory.append(reward)
            return pGrads, vGrads, nextState, d

    def acceptGrads(self, pGrads, vGrads):
        with self.graph.as_default():
            try:
                tPcyApplyOp = self.t_policyOp.apply_gradients(zip(pGrads, self.policyVars))
                tValApplyOp = self.t_valueOp.apply_gradients(zip(vGrads, self.criticVars))
                self.sess.run([tPcyApplyOp, tValApplyOp])
                time.sleep(random.uniform(0, 1))
                time.sleep(random.uniform(0, 1))
            except:
                self.acceptGrads(pGrads, vGrads)
                return

    def loadGraph(self, target):
        with self.graph.as_default():
            path = target.currentPath
            random.randint(0,2)
            try:
                time.sleep(random.uniform(0, 1))
                self.saver.restore(self.sess, path)
                time.sleep(random.uniform(0, 1))
            except:
                self.loadGraph(target)
                return
            random.randint(0,2)

    def predict(self, data):
        return self.sess.run(self.action, feed_dict={self.inputData: data})

d = readData(160000)
x = d[0]
y = d[1]
z = d[2]

# Part 4: Trade
def go_trade(tradeInterval=None, orderModel=None, pk=0, ttl=2, reportingTime=1, startingFund=100, startingBTC=0.1, rQ=False):
    agentID = str(uuid.uuid4())[-5:]
    logging.info("Hello, I am agent ["+str(agentID)+"]")
    logging.info("["+str(agentID)+"] "+"Assigned to thread: {}".format(threading.current_thread().name)) 
    logging.info("["+str(agentID)+"] "+"ID of process running: {}".format(os.getpid())) 
    funding = Decimal(startingFund).quantize(Decimal('.01'))
    btc = Decimal(startingBTC).quantize(Decimal('.001'))
    startTime = time.time()
    lastReport = time.time()
    unacceptedTrades = 0
    trades = 0
    prev = getPortfolioValue(btc, funding, pk)
    first = prev
    if not tradeInterval:
        tradeInterval = random.uniform(0, 5)

    while True:
        if time.time() - lastReport >= reportingTime*60:
            currentPortfolio = getPortfolioValue(btc, funding, pk)
            logging.warning("["+str(agentID)+"] "+"Funding:" + str(funding)+ "  BTC:"+ str(btc)+ "  Trades:"+ str(trades)+ "  Portfolio Value:"+str(currentPortfolio.quantize(Decimal('.01')))+ "  Change:"+ str(((currentPortfolio-prev)/prev).quantize(Decimal('.01'))*100)+"%"+ "  Change (From Start):"+ str(((currentPortfolio-first)/first).quantize(Decimal('.01'))*100)+"%")
            lastReport = time.time()
            prev = currentPortfolio
        
        order = orderModel.predict([getMarketData(pk)])[0]
        price = Decimal(order[0].item()).quantize(Decimal('.01'))
        coinAmount = Decimal(order[1].item()).quantize(Decimal('.001'))
        if rQ == True:
            orderType = random.sample([[0, 1], [1, 0]], 1)
            order = order[:2].tolist()+orderType[0]
        if order[2] > order[3]:
            logging.debug("["+str(agentID)+"] "+"Attempting Buy...")
            if funding >= price*coinAmount:
                data = {
                    "price":str(price), 
                    "coinAmount":str(coinAmount), 
                    "orderBook":0,
                    "type": 0,
                }
                r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
                funding -= price*coinAmount
                btc += coinAmount
                logging.info("["+str(agentID)+"] "+"Bidding order booked @ USDT"+str(price)+" for "+str(coinAmount)+" BTC.")
                trades += 1
            else:
                unacceptedTrades += 1

        elif order[3] > order[2]:
            logging.debug("["+str(agentID)+"] "+"Attempting Sell...")
            if btc >= coinAmount:
                data = {
                    "price":str(price), 
                    "coinAmount":str(coinAmount), 
                    "orderBook":0,
                    "type": 1,
                }
                r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
                btc -= coinAmount
                funding += price*coinAmount
                logging.info("["+str(agentID)+"] "+"Asking order booked @ USDT"+str(price)+" for "+str(coinAmount)+" BTC.")
                trades += 1
            else:
                unacceptedTrades += 1

        if time.time() - startTime >= ttl*60:
            break
        time.sleep(tradeInterval*60)
    logging.info("Agent "+"["+str(agentID)+"] "+"Done!")
    logging.info("["+str(agentID)+"] "+"is now finished with thread: {}".format(threading.current_thread().name)) 

def multithread_traders(count, tradeInterval=None, ttl=2, reportingTime=1, startingFund=100, startingBTC=0.1, rQ=False, x=None, y=None, z=None, logName="trader_TN.log"):
    print("Hello, I am the multithreaded agent spawner")
    print("ID of process running main program: {}".format(os.getpid())) 
    print("Main thread name: {}".format(threading.main_thread().name)) 

    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(message)s', filename=logName)
    logger = logging.getLogger(__name__)
    coloredlogs.install()

    e = Environment(association=0)
    a = Agent(0, e, x, y, z, batchSize=20, ptEpoch=1, ptLearningRate=(9e-8, 1e-18))
    order = a.predict([getMarketData(0)])[0]
    print(order)
    input("Ready to fire? ")

    listOfThreads = []
    for i in range(count):
        listOfThreads.append(threading.Thread(target=go_trade, args=(tradeInterval, a, 0, ttl, reportingTime, startingFund, startingBTC, rQ)))

    for thread in listOfThreads:
        time.sleep(random.uniform(0, 5))
        thread.start()

    for thread in listOfThreads:
        thread.join()

    print("All processes finished...")

multithread_traders(int(input("Agent Count: ")), None, 100000000000000, 1, 1000, 2, True, x, y, z)

# def subthread_worker(pk, e, ttl, master, ptDx, ptDy, ptDz, tLearningRate=(1e-10, 1e-13)):
#     a = Agent(1, e, ptDx, ptDy, ptDz, tLearningRate=tLearningRate, master=master)
# #     startingTime = time.time()
#     endingTime = startingTime+ttl
#     currentState = e.getInitialState()
#     while True:
#         st = time.time()
#         pGrads, vGrads, nextState, d = a.calcGrads(currentState)
#         master.acceptGrads(pGrads, vGrads)
#         a.loadGraph(master)
#         currentState = nextState
#         if d:
#             break
#         if time.time() >= endingTime:
#             break
#         et = time.time()

# def multithread_start_workers(agentCount, ttl, dataX, dataY, dataZ, batchSize=20, ptEpoch=1, ptLearningRate=(9e-8, 1e-14), tLearningRate=(1e-10, 5e-10), paths="D:\\Users\\kmliu\\Documents\\Developing Projects\\trader\\Trained Agents\\saved_models\\test"):
#     resetAndInitialize(1)
#     threads = []
#     for pk in range(1, agentCount+1):
#         threads.append(threading.Thread(target=subthread_worker, args=(pk, masterEnv, ttl, masterAgent, dataX, dataY, dataZ, tLearningRate)))
#     for thread in threads:
#         thread.start()
#         time.sleep(1)
#     for thread in threads:
#         thread.join()

