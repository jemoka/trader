import os
import csv

def load_agent_train_data(n=None):
    """
    Loads the agent training data from the database

    Inputs:
        optional int: sample count
    
    Returns:
        2dxn array: enumerate through data count, each contating 1dx206 input item
        2dxn array: enumerate through data count, each contating 1dx2 output item (price, amount)
        2dxn array: enumerate through data count, each contating 1dx2 output item (order type)
    """

    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "agentTrainingData.inputs.csv"), 'r', newline='') as inputDataFile, open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "agentTrainingData.outputs.csv"), 'r', newline='') as ouputDataFile:
        inputReader = csv.reader(inputDataFile, delimiter=',', quotechar='|')
        outputReader = csv.reader(ouputDataFile, delimiter=',', quotechar='|')

        regressionInputData = []
        outputData = []
        priceData = []
        orderData = []

        c = 0

        if n == None:
            for inputDataItem, outputDataItem in zip(inputReader, outputReader):
                idP1 = inputDataItem[1:4]
                idP2 = inputDataItem[7:]
                regressionInputData.append(idP1+idP2)
                outputData.append(outputDataItem)
                priceData.append(outputDataItem[:2])
                orderData.append(outputDataItem[2:])
        else:
            for inputDataItem, outputDataItem in zip(inputReader, outputReader):
                if n < c:
                    break
                else:
                    c += 1
                idP1 = inputDataItem[1:4]
                idP2 = inputDataItem[7:]
                regressionInputData.append(idP1+idP2)
                outputData.append(outputDataItem)
                priceData.append(outputDataItem[:2])
                orderData.append(outputDataItem[2:])

        return regressionInputData, outputData
