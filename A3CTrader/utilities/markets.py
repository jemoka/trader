# (c) 2019 Houjun Liu
# le little Trader | Menlo Park, CA
# Market Tools
# pylint: disable=maybe-no-member
# pylint: disable=import-error

import requests
import urllib
import json

def new(pk):
    """
    Creates market with a specific pk and populate it with Poloniex data

    Input:
        int: Market PK

    Returns none
    """
    marketURL = 'http://127.0.0.1:8000/api/USDT_BTC/markets/?pk='+str(pk)
    requests.post(marketURL)

def delete(pk):
    """
    Removes market with a specific pk

    Input:
        int: Market PK

    Returns none
    """
    marketURL = 'http://127.0.0.1:8000/api/USDT_BTC/markets/?pk='+str(pk)
    requests.delete(marketURL)

def get_ticker(pk):
    """
    Gets the ticker data for a specific market

    Input:
        int: Market PK

    Outputs:
        1dx6 array: [float last, float lowest_ask, float highest_bid, float percent_change, float low_24, float high_24]
        OR
        str: error
    """
    tickerUrl = 'http://127.0.0.1:8000/api/USDT_BTC/ticker/?pk='+str(pk)
    tickerData = requests.get(tickerUrl).json()
    try:
        tickerData["last"]
    except:
        return str(tickerData["detail"])
    return [float(tickerData["last"]), float(tickerData["lowestAsk"]), float(tickerData["highestBid"]), float(tickerData["percentChange"]), float(tickerData["high24hr"]), float(tickerData["low24hr"])]

def get_value_eval(coin, pk):
    """
    Gets the market price evaluation for an amount of coin.

    Inputs:
        float: coin amount
        int: OrderBook PK

    Outputs:
        float: market price evaulation
        OR
        str: error
    """

    tickerUrl = 'http://127.0.0.1:8000/api/USDT_BTC/ticker/?pk='+str(pk)
    tickerData = requests.get(tickerUrl).json()
    try:
        tickerData["detail"]
    except:
        return str(tickerData["detail"])
    return float(tickerData["last"]*coin)

def get_smallest_unused():
    """
    Gets the smallest unused market pk

    Inputs none
    Outputs:
        int: market PK
    """
    pk = 0
    ticker = get_ticker(pk)
    while type(ticker) != str:
        pk += 1
        ticker = get_ticker(pk)
    return pk

def load_human_order(ob, n, tp):
    """
    Loads orders from Poloniex as supply

    Inputs:
        int: orderbook PK
        n: number of orders to be fufilled
        tp: order type (0=>ask, 1=>bid)
    """

    with urllib.request.urlopen('https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC') as response:
        orderBook = json.load(response)

    if tp == 0:
        asks = orderBook["asks"][:n]
        bids = []
    elif tp == 1:
        bids = orderBook["bids"][:n]
        asks = []

    for i in asks:
        price = float(i[0])
        amount = i[1]

        data = {
            "price":round(price, 2), 
            "coinAmount":round(amount, 10), 
            "orderBook":ob,
            "type": 2,
        }

        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 

    for i in bids:
        price = float(i[0])
        amount = i[1]

        data = {
            "price":round(price, 2), 
            "coinAmount":round(amount, 10), 
            "orderBook":ob,
            "type": 3,
        }

        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data)
    


def get_order_book(pk, tp):
    """
    Gets the orderbook of pk of type 0 (asks), 1 (bids)

    Inputs:
        int: Orderbook PK
        int: 0 => buy; 1 => sell

    Outputs:
        1dx50 array: [[float price, float amount], **kw]
    """

    marketUrl = 'http://127.0.0.1:8000/api/USDT_BTC/markets/'
    request = requests.get(marketUrl)

    if tp == 0:
        ob = [[float(y) for y in x] for x in request.json()[0]["orderBooks"][0]["asks"]][:50]
        while len(ob) < 50:
            load_human_order(pk, 1, 0)
            request = requests.get(marketUrl)
            ob = [[float(y) for y in x] for x in request.json()[0]["orderBooks"][0]["asks"]][:50]
        return ob
    elif tp == 1:
        ob = [[float(y) for y in x] for x in request.json()[0]["orderBooks"][0]["bids"]][:50]
        while len(ob) < 50:
            load_human_order(pk, 1, 1)
            request = requests.get(marketUrl)
            ob = [[float(y) for y in x] for x in request.json()[0]["orderBooks"][0]["asks"]][:50]
        return ob

def post_order(order, orderBook):
    """
    Posts and order for use in testing.

    Inputs: 
        1dx4 array: [float_price, float_amount, int_buy, int_sell]
        int: OrderBook PK
    
    Outputs:
        tuple: (int_status, response)
    """

    if order[2] > order[3]:
        data = {
            "price":str(order[0]), 
            "coinAmount":str(order[1]), 
            "orderBook":orderBook,
            "type": 0,
        }
        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
        try:
            r.raise_for_status()
            return (0, r)
        except requests.exceptions.HTTPError:
            # Whoops it wasn't a 200
            return (1, r)
        
    elif order[3] > order[2]:
        data = {
            "price":str(order[0]), 
            "coinAmount":str(order[1]), 
            "orderBook":orderBook,
            "type": 1,
        }
        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 
        try:
            r.raise_for_status()
            return (0, r)
        except requests.exceptions.HTTPError:
            # Whoops it wasn't a 200
            return (1, r)

